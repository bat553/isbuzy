/*
    datas
    isbuzy

    Created by Swano 02/03/19 22:46
*/

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var MacListSchema = new Schema({
	mac_address: String,
	utc_time:{
		required: true,
		type: Date,
		default: function () {
			return new Date()
		},
		unique: true
	}
});

exports.MacListSchema = mongoose.model('MacListSchema', MacListSchema, 'mac_list');