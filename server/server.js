/*
    server
    isbuzy

    Created by Swano 02/03/19 22:45
*/

require('dotenv').load();
var morgan = require('morgan');
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var cors = require('cors');
var DataSet = require('./model/datas');
var MacListSchema = DataSet.MacListSchema;

var app =	express();

var port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(morgan(process.env.NODE_ENV || 'dev'));

app.use(cors({
	methods: "GET",
	maxAge: 86400
}));

mongoose.connect("mongodb://mongodb:27017/isbuzy",  function (err) {
	if (err) return console.log(err);
	console.log("Api version 0.1");

	app.post('/add_mac', function (req, res) {
		mac = new MacListSchema({
			mac_address : req.body.mac_address
		});
		mac.save(function (err, result) {
			if (err || !result){
				res.status(512).send()
			} else {
				res.status(201).send({success: true, mac: req.body.mac_address})
			}
		})
	});


	app.get("/agg/:minutes", function(req, res){
		// Par groupe de n minutes
		if (!req.params.minutes){
			res.status(400).send({success: false, msg: "Merci de spécifier le nbr de minutes"})
		} else {
			MacListSchema.aggregate([
				{
					"$group": {
						"_id": {
							"$toDate": {
								"$subtract": [
									{"$toLong": "$utc_time"},
									{"$mod": [{"$toLong": "$utc_time"}, 1000 * 60 * req.params.minutes]}
								]
							}
						},
						"count": {"$sum": 1}
					}
				}
			], function (err, agg) {
				// Merci : https://stackoverflow.com/questions/26814427/group-result-by-15-minutes-time-interval-in-mongodb
				if (!agg || err){
					res.status(512).send({success: false})
				} else {
					res.status(200).send({success: true, doc: agg})

				}
			})
		}
	});


	app.get("/", function (req,res) {
		res.status(200).send({success: true, msg: "Welcome to the api!", commit: process.env.COMMIT || "notfound"})
	});

	app.use(function (req, res) {
		// Erreur 404
		res.status(404).send({success: false, err: 404})
	});

	app.listen(port, function () {
		console.log("Server started on : " + port)
	});



});