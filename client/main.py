"""
Baptiste Pellarin 2019
baptiste.pellarin@gmail.com
"""

import pyshark
import requests
import time
import os

cap = pyshark.LiveCapture(interface='mon0', bpf_filter="ether host not ff:ff:ff:ff:ff:ff")
cap.sniff(timeout=15)
cap

mac_list_path = '/tmp/mac_list.txt'

future = int(time.time()) + 5
def reset_maclist():
        os.remove(mac_list_path)
        mac_list_write = open(mac_list_path, "w")
        mac_list_write.write(str(time.time()) + "\n") ## Le temps est sur la permiere ligne
        mac_list_write.close()
        print("============================")
        print("============RESET===========")
        print("============================")
def init():
        mac_list_write = open(mac_list_path, "w")
        mac_list_write.write(str(time.time()) + "\n") ## Le temps est sur la permiere ligne
        mac_list_write.close()
        print("============================")
        print("============INIT============")
        print("============================")
init()

for packet in cap.sniff_continuously():
        mac_addr = str(packet.wlan.addr)
        mac_list_read = open(mac_list_path, "r+")
        if mac_addr not in mac_list_read.read():
                print('Just arrived:', mac_addr)
                # Si l@mac n'est pas deja connu
                # Ajouter au fichier
                mac_list_read.write(mac_addr + "\n")
                # Ajouter à la bdd
                r = requests.post("http://192.168.10.60:8090/add_mac", data = {"mac_address": mac_addr})
                print(r.text)

        mac_list_read.close()

        if (time.time() >= future):
                # Reset des @mac Toutes les cinq minutes
                reset_maclist()
                future = int(time.time()) + 300



